<?php

	// config:
	$PASSWORD='YOURPASSWORD';
	date_default_timezone_set('Europe/Paris'); // because french baguette omelette du fromage

	// disconnect
	if(isset($_GET['deco'])){
		setcookie('gné', '',time()-5);
		setcookie("expire","a",time()-5);
		header('Location: index.php');
	}

	// connect
	if(isset($_POST['password']) && $_POST['password'] == $PASSWORD){
		setcookie('gné', $PASSWORD, time()+60*60*2); // not very secure ?
		setcookie("expire",time()+60*60*2,time()+60*60*2);
		header('Location: index.php');
	}


	if(isset($_GET['unlink']) && $_GET['unlink'] != "index.php"){
		if(isset($_COOKIE["gné"]) && $_COOKIE['gné'] == $PASSWORD){
			unlink($_GET['unlink']);
			header('Location: index.php');
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Up</title>
	<meta name="viewport" content="width=device-width" />
	<link rel="stylesheet" href="../design.css" />
</head>
<body>
	<h1>Afficheur hypersimple <?php if(isset($_COOKIE['gné'])){ echo "<sup><a href=\"?deco\">X</a></sup>"; } ?></h1>

	<?php
	if(isset($_COOKIE['gné']) && $_COOKIE['gné'] == $PASSWORD){
		echo "<p>Liste des fichiers hébergés :</p>";
		if(!isset($_GET['antisort'])){
			echo "<p><small><a href=\"?antisort\">Trier du plus ancien au plus récent</a>.</small></p>";
		}
		else{
			echo "<p><small><a href=\"index.php\">Trier du plus récent au plus ancien</a>.</small></p>";
		}
		$nb_fichier = 0;
		echo '<ul>';
		if($dossier = opendir('../files')){

			while(false !== ($fichier = readdir($dossier))){
				if($fichier != '.' && $fichier != '..' && $fichier != 'index.php'){
					$fichiers[] = $fichier;
					$nb_fichier++; // On incrémente le compteur de 1
				} // On ferme le if (qui permet de ne pas afficher index.php, etc.)
			} // On termine la boucle

			sort($fichiers);
			if(!isset($_GET['antisort'])){
				$fichiers = array_reverse($fichiers);
			}
			foreach ($fichiers as $fichier){
				echo '<li><a class="link" href="./' . $fichier . '">' . $fichier . '</a><div class="unlink"><span class="infotext">?<span class="infobulle">'.  date("d/m/Y \à H:i:s", filemtime($fichier)) .'</span></span> <a href="index.php?unlink='.$fichier.'">X</a></div></li>';
			}

			echo '</ul><br />
			<p>Il y a <strong>' . $nb_fichier .'</strong> fichier(s) dans le dossier.</p>';

			closedir($dossier);
		}
		else{
		   	 echo 'Le dossier n\' a pas pu être ouvert';
    	}

    	echo "<p><a href=\"../index.php\">Retour sur l'hébergeur</a>.<br /><small>Autodéco dans ". date("H:i:s", $_COOKIE['expire']-time()) .".</small></p>";
    }
    else{
    	?>
    	<form method="post" action="index.php">        
    		<label>Mot de passe :</label><input type="password" name="password"><br>
    		<label></label><input type="submit" value="Voir les fichiers"> 
		</form>
		<p>
			<a href="../index.php">Retour sur l'hébergeur</a>.
		</p>
    	<?php
    }
	?>
</body>
</html>