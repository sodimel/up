# Hébergeur hypersimple

## _une version modifiée du [filehosting](http://sebsauvage.net/wiki/doku.php?id=php:filehosting) de [sebsauvage](http://sebsauvage.net/)_



#### Installation (en anglais mais c'est pas dur à comprendre):
* Downdload [zip file](https://gitlab.com/sodimel/up/repository/archive.zip?ref=master).
* Extract zip file.
* Modify passwords (index.php **and** files/index.php).
* Host the files (except README.md) on your website (generally on a subdir).
* Enjoy.




Alors **à la base**, c'était vraiment hypersimple.  
#### Mais j'ai très très vite réussi à tout compliquer, donc voici la liste des features du projet:
* Hébergement de fichiers de tous types dans le sous-dossier `files/`.
* Protection de l'hébergement par mot de passe.
* Hébergement d'images contenues dans le presse-papier en appuyant sur ctrl+v (si le mot de passe est rentré ou bien si l'utilisateur est connecté).
* Cookie de connexion d'une durée de deux heures (pour éviter de retaper le mot de passe tout le temps).
* Visionnage des fichiers hébergés par n'importe qui, pour peu qu'il/elle dispose de l'adresse dudit fichier.
* Liste des fichiers hébergés accessible par mot de passe.
* Possibilité de supprimer un fichier hébergé depuis la liste.
* Possibilité de récupérer la date d'upload des fichiers depuis la liste.
* Possibilité d'afficher la liste des fichiers du plus ancien au plus récent (et inversement).


### Screenshots:
#### Accueil:
![accueil](http://l3m.in/p/up/files/1497214870.png)

#### Liste des fichiers:
![liste](http://l3m.in/p/up/files/1497288892.png)